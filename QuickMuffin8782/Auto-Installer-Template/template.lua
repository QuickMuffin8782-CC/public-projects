function autoShell(cmd)
	term.setTextColor(colors.yellow)
	write("> ")
	term.setTextColor(colors.white)
	textutils.slowWrite(cmd, 50)
	print("")
	shell.run(cmd)
end

function ask(msg)
	local x, y = term.getCursorPos()
	while true do
	term.setCursorPos(1, y)
	term.clearLine()
	write(msg .. " ")
	local out = read(nil)
	if out:len() == 1 then
		if out == "y" then
			return true
		elseif out == "n" then
			return false
		end
	end
	end
end

if ask("Auto install <program name> to this computer?") then
else
printError("Canceled installation.")
end
