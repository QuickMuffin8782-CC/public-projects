local program = require "multitask"
local expect = require("cc.expect").expect

local alt, shift, ctrl = false, false, false
local w, h = term.getSize()

local function cwrite(txt, y)
  term.setCursorPos((w/2)-(#txt/2), y)
  term.write(txt)
end

local function drawMsg(txt)
  term.setBackgroundColor(colors.gray)
  term.setTextColor(colors.white)
  term.setCursorPos(1, (h/2)-2)
  term.clearLine()
  term.setCursorPos(1, (h/2)-1)
  term.clearLine()
  term.setCursorPos(1, (h/2))
  term.clearLine()
  term.setCursorPos(1, (h/2)+1)
  term.clearLine()
  term.setCursorPos(1, (h/2)+2)
  term.clearLine()
  cwrite(txt, (h/2))
end

programs = {
  program.new(function()
    while true do
      local w, h = term.getSize()
      term.setBackgroundColor(colors.white)
      term.setTextColor(colors.black)
      term.clear()
      term.setCursorPos((w/2)-((w..", "..h):len()/2) + 1, (h/2) + 1)
      term.write(w..", "..h)
      coroutine.yield()
    end
  end, 2, 3, 20, 10),
  program.new(function()
    while true do
      local w, h = term.getSize()
      term.setBackgroundColor(colors.lightBlue)
      term.setTextColor(colors.black)
      term.clear()
      term.setCursorPos((w/2)-((w..", "..h):len()/2) + 1, (h/2) + 1)
      term.write(w..", "..h)
      coroutine.yield()
    end
  end, 2, 23, 20, 10)
}

local function addWindow(func, x, y, w, h)
  expect(1, func, "function")
  expect(2, x, "number")
  expect(3, y, "number")
  expect(4, w, "number")
  expect(5, h, "number")
  table.insert(programs, program.new(func, x, y, w, h))
end

function updateKeyInfo(ev, key)
  if ev == "key" then
    if key == keys.rightShift then shift = true end
    if key == keys.leftShift then shift = true end
    if key == keys.rightCtrl then ctrl = true end
    if key == keys.leftCtrl then ctrl = true end
    if key == keys.rightAlt then alt = true end
    if key == keys.leftAlt then alt = true end
  end
  if ev == "key_up" then
    if key == keys.rightShift then shift = false end
    if key == keys.leftShift then shift = false end
    if key == keys.rightCtrl then ctrl = false end
    if key == keys.leftCtrl then ctrl = false end
    if key == keys.rightAlt then alt = false end
    if key == keys.leftAlt then alt = false end
  end
end

while true do
  local event, var1, var2, var3 = os.pullEventRaw()
  w, h = term.getSize()
  if event == "key" then
    if ctrl and alt and var1 == keys.o then
      drawMsg("Thanks for using!")
      sleep(1.25)
      break
    end
  elseif event == "terminate" then
    if #programs < 1 then
      drawMsg("To close, press Ctrl+Alt+O.")
      sleep(1.25)
    end
  end
  term.setBackgroundColor(colors.blue)
  term.clear()
  program.update(programs, event, var1, var2, var3)
  updateKeyInfo(event, var1)
end

term.setBackgroundColor(colors.black)
term.clear()
term.setCursorPos(1, 1)
