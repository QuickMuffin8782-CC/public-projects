# Contributing to the public projects

By contributing you agree to what you are creating in this repository.

Contributing requires a account, which you can create if you don't have one. (You can also use google and github as well)

Once you have created your account, you can be able to contribute with your creations in your assigned directory. If you want, create your directory with your username in the modules section to create a module to work with in the enviornment of ComputerCraft or MineOS.