{
    ["QuickMuffin8782"] = {
        ["installer-maker-CC"] = {
            name = "Installer Maker for ComputerCraft",
            files = {}
        }
    },
    ["ZombieNerd1001"] = {
        [1] = { -- Change the "1" with your project name
            name = nil, -- Change with your project name
            files = { -- Change with your project files in this gitlab so this can install, you can add more just add the "," at the end before you add another one. It is like [<string: install to directory>] = <string: url to file>
           
        }
    },
    ["Memeycat26"] = {},
    ["Bombcity295"] = {},
    ["bookie1228"] = {}, -- ZombieNerd's friends are welcome as well, just make sure they add their directory.
    ["RodzinkaLusi"] = {}, -- A friend of mine on MCBE, haven't played with them a long time.
    ["SamH"] = {
        ["CCBrowser"] = {
            name = "CCBrowser v1.0.0",
            files = {
                ["browser.lua"] = "ccbrowser.lua"
            }
        },
        ["CCBrowser-dev/2.0.0"] = { --< Is this how it works??? >-- --< Reply: Yeah, that's how it works! >--
            name = "CCBrowser v2.0.0",
            files = {
                ["browser.lua"] = "main.lua"
            }
        },        
    }, -- New contributor for SamH's projects.
    ["Other"] = {}, -- Other projects (third party stuff and whatnot)
    ["Modules"] = {} -- Modules to install 
}
