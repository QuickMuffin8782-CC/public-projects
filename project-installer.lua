if not fs then
    print("This is for computercraft only. Check again for availability")
    return
end

local function get(sUrl)
    -- Check if the URL is valid
    local ok, err = http.checkURL(url)
    if not ok then
        return "err:" .. (err or "invalid")
    end

    local response = http.get(sUrl , nil , true)
    if not response then
        print("Failed.")
        return "err:failed"
    end

    local sResponse = response.readAll()
    response.close()
    return sResponse
end


local h = http.get("https://www.gitlab.com/QuickMuffin8782-CC/public-projects/-/raw/master/project-list.lua")


local mainDir = "" -- Do not remove or it will break.
local mainUrl = "https://www.gitlab.com/QuickMuffin8782-CC/public-projects/-/raw/master/"

local function installFile(url, dir)
    local installUrl = url
    if installUrl:gsub(1, 1) == "/" then
        installUrl = installUrl:gsub(2, string.len(installUrl))
    end
    verbosePrint("Opening file at " .. dir)
    local file = io.open(dir, "w")
    verbosePrint("Getting contents of " .. installUrl .. "...")
    local urlContents = get(mainUrl .. installUrl)
    if urlContents:sub(1,4) == "err:" then
        verbosePrint("File error, retrying...")
        while true do
            urlContents = get(mainUrl .. installUrl)
            if not urlContents:sub(1,4) == "err:" then
                break
            end
            verbosePrint("Failed, trying again...")
        end
        verbosePrint("Got file, but not successfully at the start")
    end
    verbosePrint("Writing contents of " .. installUrl .. " to " .. dir .. "...")
    file:write(urlContents)
    verbosePrint("Done, closing file...")
    file:close()
end

local verbose = false

function verbosePrint(txt, type)
    if verbose then
        if type == "error" then
            printError("[ERROR] " .. txt)
        elseif type == "warning" then
            printError("[WARNING] " .. txt)
        else
            print(txt)
        end
    end
end

-- Install a project by the arguments
local args = {...}

if #args < 1 then
    print("Specify a project or module. If needed type \"help\" along with this program.")
    return
end

if #args < 3 then
    if args[1] == "help" then
        print([[Public Project Installer

Install a module: ]] .. shell.getRunningProgram() .. [[ module <user> <module>
Install a project: ]] .. shell.getRunningProgram() .. [[ project <user> <project>

You can also append -v at the end of the arguments to start a verbose print.

This is part of ComputerCraft. This uses gitlab to install.]])
    else
        printError("Can't be one or two arguments. Type \"help\" to figure out what to do.")
    end
    return
end

--[[function doesExist(user, project)
    return projects[user][project].files
end]]

function install(user, project, dir)
    verbosePrint("Installing files...")
    for a, b in pairs(projects[user][project].files) do
        installFile(b, dir .. a)
        verbosePrint("Installed to " .. a .. ", heading onto next possible file to download...")
    end
    verbosePrint("Finished installing files")
end

local function programInit()
if #args > 2 then
    if args[3] == "-v" then
        verbose = true
    end
    print("Enter a directory to install module/project")
    write("dir: ")
    mainDir = read() -- Directory system, this will be able to use for autocomplete. Once done, it'll install at the MAIN DIRECTORY! Do not remove the local variable or it will break.
    if args[1] == "module" then
        --[[if not doesExist("Modules", args[2] .. "/" .. args[3]) then
            printError("This module doesn't exist in the list, or there is no files specified. If possible, check your spelling and try again.")
            return
        end]]
        install("Modules", args[2] .. "/" .. args[3], (mainDir and mainDir or "/") .. "modules")
    elseif args[1] == "project" then
        --[[if not doesExist(args[2], args[3]) then
            printError("This project doesn't exist in the list, or there is no files specified. If possible, check your spelling and try again.")
            return
        end]]
        install(args[2], args[3], (mainDir and mainDir or "/"))
    end
end
end

local ok, err = pcall(programInit)

if not ok then
    printError(err)
    printError("You might of broke the program or a bug has occurred. Check the code or find the cause if applicable.")
end
